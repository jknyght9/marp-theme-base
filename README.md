# Marp Base Theme

This project contains a base theme for Marp-based presentations. The `.vscode` folder must remain at the root of your course directory. You may need to change the `.vscode/marp-theme/blue/theme.css` image paths, depending on how deep your Marp slide deck is; otherwise the backgrounds may not appear correctly.

For example, if your slides.md file is in Module 1/Awesome Lessson/slides.md, you will need to start from that location and traverse your way to the target background image. Make your slides.md see the right path to the image. 

```shell
# Example
../../.vscode/marp-theme/blue/background_dark.png
```

## Marp Frontmatter

```yaml
---
title: My Course
description: My Description
author: My Name
date: 01/12/2023
marp: true
theme: blue
size: 16:9
paginate: true
footer: "© 2023-2024 Author, All Rights Reserved"
---
```

## Edits to CSS

**Do not** make changes to the base.css directly. Make all edits to the `base.scss` file and recompile the SASS; otherwise, the two will be out of sync and may break future workflows.